#!/bin/bash
#
# Cleanup branches corresponding to completed Jira tickets.
#
# The output is a list of git commands for branches that are no
# longer needed. You could pipe the output into shell for execution,
# but it's better to capture to a temporary file for review prior to run.
#
# Usage: cleanup_branches >tmp ; bash -x tmp
#

set -o errexit pipefail

# Load user-specific definitions for the Atlassian API.
API_DEFS_FILE="${HOME}/.atlassian-api.defs"
. ${API_DEFS_FILE}
: ${API_USER:?} ${API_TOKEN:?} ${API_BASE_URL:?}
if [[ "$API_TOKEN" == "not.configured.yet" ]] ; then
  echo "${API_DEFS_FILE}: configuration settings need to be adjusted -- see project README.md" >&2
  exit 1
fi

if [[ ! -d .git ]] ; then
  echo "$0: the current directory is not a git project" >&2
  exit 1
fi


#############################################################################
#
# functions
#

# get a list of local branches, with formatting removed
list_branches() {
  git branch --no-color | sed -e 's/^\*//' -e's/^ *//'
}

# convert something like "CONTROL-314[-something-more]" to "CONTROL-314"
# things that don't look like a ticket (such as "main") will return an empty string
get_ticket_for_branch() {
  echo "$1" | sed -n -E 's/^([^-]+-[0-9]+).*/\1/p'
}


# expected response is one of: "To Do", "In Progress", "Done", or "null"
get_status_category_for_ticket() {
  curl --user "${API_USER}:${API_TOKEN}" --silent "${API_BASE_URL}/rest/api/latest/issue/${1}" \
    | jq .fields.status.statusCategory.name \
    | sed -e 's/"//g'
}


#############################################################################
#
# main
#

list_branches | while read branch ; do

  # convert "CONTROL-1234[-something-more]" => "CONTROL-1234"
  ticket=$(get_ticket_for_branch "$branch")

  # skip branches that don't have a ticket associated (such as "main")
  if [[ -z ${ticket} ]] ; then
    echo "${branch} SKIP" >&2
    continue
  fi

  # value will be: "To Do", "In Progress", "Done", or "null"
  status_category=$(get_status_category_for_ticket "$ticket")

  echo "${branch} (${ticket}) ${status_category}" >&2

  # emit git command if this branch should be deleted AND we are capturing stdout (i.e. not a tty)
  if [[ ! -t 1 && "$status_category" == "Done" ]] ; then
    echo "git branch -D ${branch}"
  fi

done
