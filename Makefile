BINDIR = $(HOME)/bin

all :

install : $(BINDIR) $(BINDIR)/cleanup-branches $(HOME)/.atlassian-api.defs

$(BINDIR) :
	mkdir -p $(BINDIR)

$(BINDIR)/cleanup-branches : cleanup-branches.sh
	install -m 555 $< $@

$(HOME)/.atlassian-api.defs :
	install -m 700 atlassian-api.defs.example $@
	@echo "NOTICE: be sure to edit your $@ file -- see README.md for help"
