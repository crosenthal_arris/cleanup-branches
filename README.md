# cleanup-branches -- Cleanup branches corresponding to completed atlassian tickets.

Often times, ECO developers will create a branch per ticket, where the branch
name starts with the ticket number (such as "CONTROL-31415"). Those branches can
accumulate as work proceeds.

## Installation

### 1. Install

First, run the "make" command to install the utility:

    $ make install
    install -m 555 cleanup-branches.sh /home/courtney/bin/cleanup-branches
    install -m 700 atlassian-api.defs.example /home/courtney/.atlassian-api.defs
    NOTICE: be sure to edit your /home/courtney/.atlassian-api.defs file -- see README.md for help

### 2. Generate API token

Next, generate an Atlassian API token. 

Create an API token here:
https://id.atlassian.com/manage-profile/security/api-tokens

*IMPORTANT!* Your API token is displayed only once. Be sure to capture it so 
you can use it below. If you lose your API token then use this page to 
revoke the old token and issue a new one.

### 3. Configurure API settings

Finally, edit your _~/atlassian-api.defs_ file.

* API_USER -- set to your Atlassian login username (typically your email address)
* API_TOKEN -- set to the API token you generated above
* API_BASE_URL -- leave unchanged for ECO projects

## Usage

Run _cleanup-branches_ from the base directory of your ECO project.

It will emit the following output:

* stderr - details on all of the branches found, in format "BRANCH (TICKET) 
  STATUS"
* stdout - a list of git commands to delete the branches that are no longer 
  needed (they have a status that is done)

You could pipe the output of the script of the script directly into a shell, 
but it's better to capture it to a file so you can inspect the actions. Then 
you can run that file as a script.

Example:

    $ cleanup-branches >tmp
    CONTROL-1487 (CONTROL-1487) Done
    CONTROL-1635 (CONTROL-1635) Done
    CONTROL-1635.backup01 (CONTROL-1635) Done
    CONTROL-1655 (CONTROL-1655) In Progress
    CONTROL-1768 (CONTROL-1768) Done
    CONTROL-1950 (CONTROL-1950) In Progress
    
    $ cat tmp
    git branch -D CONTROL-1487
    git branch -D CONTROL-1635
    git branch -D CONTROL-1635.backup01
    git branch -D CONTROL-1768
    
    $ bash  tmp
    Deleted branch CONTROL-1487 (was 6e39a4f).
    Deleted branch CONTROL-1635 (was 412984c).
    Deleted branch CONTROL-1635.backup01 (was 21fe339).
    Deleted branch CONTROL-1768 (was 168beae).
    
    $ rm tmp
